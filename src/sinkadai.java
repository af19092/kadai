import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class sinkadai {
    private JPanel root;
    private JButton ramenButton;
    private JButton karaageButton;
    private JTextPane textPane2;
    private JButton gyouzaButton;
    private JButton yakisobaButton;
    private JButton tempuraButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton checkoutButton;
    private JTextPane textPane3;

    int sum=0;
    void confirmation(String food, int y) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            /*★*/JOptionPane.showMessageDialog(null,"order for "+food+" received");
            String currenttext = textPane1.getText();/*テキストを表記★*/
            textPane1.setText(currenttext+food+"\n");/*テキストに過去に入力したデータ残しながら表記*/
            String currenttext2=textPane2.getText();
            textPane2.setText(currenttext2+y+"\n");

            sum=sum+y;
            textPane3.setText(sum+"\n");


    }

}
    public sinkadai() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("tempura", 650);
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("udon", 700);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("ramen", 500);}});
                ramenButton.setIcon(new ImageIcon(
                        this.getClass().getResource("ramen.jpg")
                ));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("karaage", 550);
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")
        ));
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("tempura", 350);
            }
        });
        gyouzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyouza.jpg")
        ));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                confirmation("yakisoba", 850);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakisoba.jpg")
        ));
        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation2 = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out? ",
                        "Order confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation2 == 0) {
                    /*★*/
                    JOptionPane.showMessageDialog(null, "Thank you! the total price is" +
                            ""+sum+"yen");
                    textPane1.setText(   "\n");
                    textPane2.setText(    "\n");
                    sum=0;
                    textPane3.setText(    "\n");

                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("sinkadai");
        frame.setContentPane(new sinkadai().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}